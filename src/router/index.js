import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '../views/login.vue'
import account from '../views/account.vue'
import chat from '../views/chat.vue'
import chat2 from '../views/chat2.vue'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'login',
    component: login
  },
  {
    path: '/account',
    name: 'account',
    component:account
  },
  {
    path: '/chat',
    name: 'chat',
    component:chat
  },
  {
    path: '/chat2',
    name: 'chat2',
    component:chat2
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
